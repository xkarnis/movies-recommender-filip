package cz.muni.pa165.rating.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.util.CommonUtils;
import cz.muni.pa165.rating.data.model.MovieRating;
import cz.muni.pa165.rating.data.repository.MovieRatingRepository;
import java.util.HashSet;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DataSeederService {

    private final MovieRatingRepository movieRatingRepository;

    @Value("${rest.data.seed.ratings}")
    private String ratingsSeedFile;

    @Autowired
    public DataSeederService(MovieRatingRepository movieRatingRepository) {
        this.movieRatingRepository = movieRatingRepository;
    }

    public void clearData() {
        movieRatingRepository.deleteAll();
        movieRatingRepository.flush();
    }

    public void seedData() {
        clearData();
        List<MovieRating> ratings = CommonUtils.loadSeedJson(
            new ObjectMapper(),
            ratingsSeedFile,
            new TypeReference<>() {}
        );

        //for now just set the same movie and user ids for each rating
        ratings.forEach(movieRating -> movieRating.setMovieId(1L));
        ratings.forEach(movieRating -> movieRating.setUserId(1L));

        movieRatingRepository.saveAll(ratings);
    }
}
