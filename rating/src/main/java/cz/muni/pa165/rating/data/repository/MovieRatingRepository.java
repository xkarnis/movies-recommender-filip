package cz.muni.pa165.rating.data.repository;

import cz.muni.pa165.rating.data.model.MovieRating;
import cz.muni.pa165.rating.data.model.RatingFilter;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRatingRepository
    extends JpaRepository<MovieRating, Long> {
    @Query(
        """
        SELECT mr FROM MovieRating mr WHERE
        (:#{#filter.movieId} IS NULL OR mr.movieId = :#{#filter.movieId}) AND
        (:#{#filter.userIds} IS NULL OR mr.userId IN :#{#filter.userIds}) AND
        (:#{#filter.overall} IS NULL OR mr.overall >= :#{#filter.overall})
    """
    )
    Page<MovieRating> findAllFiltered(RatingFilter filter, Pageable pageable);
}
