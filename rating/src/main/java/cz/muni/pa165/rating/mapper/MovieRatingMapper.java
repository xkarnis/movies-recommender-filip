package cz.muni.pa165.rating.mapper;

import cz.muni.pa165.core.api.rating.MovieRatingDto;
import cz.muni.pa165.rating.data.model.MovieRating;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

@Mapper(componentModel = "spring")
public interface MovieRatingMapper {
    MovieRatingDto mapToDto(MovieRating movieRating);

    MovieRating mapToEntity(MovieRatingDto movieRatingDto);

    List<MovieRatingDto> mapToList(List<MovieRating> movieRatings);

    default Page<MovieRatingDto> mapToPageDto(Page<MovieRating> movieRatings) {
        return new PageImpl<>(
            mapToList(movieRatings.getContent()),
            movieRatings.getPageable(),
            movieRatings.getTotalPages()
        );
    }
}
