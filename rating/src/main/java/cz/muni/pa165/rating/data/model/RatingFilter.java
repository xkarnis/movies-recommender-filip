package cz.muni.pa165.rating.data.model;

import jakarta.annotation.Nullable;
import java.util.List;
import lombok.Data;

@Data
public class RatingFilter {

    @Nullable
    private Long movieId;

    @Nullable
    private List<Long> userIds;

    @Nullable
    private Integer overall;
}
