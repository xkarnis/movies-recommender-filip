package cz.muni.pa165.rating.service;

import cz.muni.pa165.core.exception.ResourceNotFoundException;
import cz.muni.pa165.rating.MockedEntities;
import cz.muni.pa165.rating.config.MockConfiguration;
import cz.muni.pa165.rating.data.model.RatingFilter;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;

@Import(MockConfiguration.class)
@ActiveProfiles("test")
@SpringBootTest
class MovieRatingServiceTest {

    private final MovieRatingService service;

    @Autowired
    public MovieRatingServiceTest(MovieRatingService service) {
        this.service = service;
    }

    @Test
    void findByIdExisting() {
        Assertions.assertSame(
            MockedEntities.RATING_1,
            service.findById(MockedEntities.RATING_1.getId())
        );
    }

    @Test
    void findByIdNotExisting() {
        Assertions.assertThrows(
            ResourceNotFoundException.class,
            () -> service.findById(5L)
        );
    }

    @Test
    void findAll() {
        Assertions.assertEquals(
            new PageImpl<>(MockedEntities.RATINGS),
            service.findAll(new RatingFilter(), PageRequest.of(0, 5))
        );
    }

    @Test
    void create() {
        Assertions.assertSame(
            MockedEntities.RATING_1,
            service.create(MockedEntities.RATING_1)
        );
    }

    @Test
    void update() {
        Assertions.assertSame(
            MockedEntities.RATING_1,
            service.update(MockedEntities.RATING_1)
        );
    }

    @Test
    void delete() {
        service.delete(MockedEntities.RATING_1);
    }
}
