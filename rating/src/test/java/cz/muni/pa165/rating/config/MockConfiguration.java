package cz.muni.pa165.rating.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.api.account.AccountDto;
import cz.muni.pa165.core.exception.ResourceNotFoundException;
import cz.muni.pa165.rating.MockedEntities;
import cz.muni.pa165.rating.data.model.RatingFilter;
import cz.muni.pa165.rating.data.repository.MovieRatingRepository;
import cz.muni.pa165.rating.service.MovieRatingService;
import cz.muni.pa165.rating.service.WebClientAccountService;
import jakarta.annotation.PostConstruct;
import java.io.IOException;
import java.util.Optional;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@TestConfiguration
public class MockConfiguration {

    @Autowired
    private ObjectMapper objectMapper;

    @Mock
    private MovieRatingRepository repository;

    private final MockWebServer mockBackEnd = new MockWebServer();

    @PostConstruct
    public void setup() throws IOException {
        MockitoAnnotations.openMocks(this);
        mockRepositoryMethods();
        mockBackEnd.start();

        AccountDto mockAccount = new AccountDto(
            1L,
            "John Wick",
            "john@wick.com",
            false
        );

        mockBackEnd.enqueue(
            new MockResponse()
                .setBody(objectMapper.writeValueAsString(mockAccount))
                .addHeader("Content-Type", "application/json")
        );
    }

    @Bean
    public WebClientAccountService webClientAccountService() {
        return new WebClientAccountService(
            "http://localhost",
            Integer.toString(mockBackEnd.getPort()),
            "/api/v1/accounts/me"
        );
    }

    @Bean
    public MockWebServer mockWebServer() {
        return new MockWebServer();
    }

    @Bean
    @Primary
    public MovieRatingService movieRatingService() {
        return new MovieRatingService(repository);
    }

    private void mockRepositoryMethods() {
        Mockito
            .doThrow(ResourceNotFoundException.class)
            .when(repository)
            .findById(Mockito.anyLong());
        Mockito
            .doReturn(Optional.of(MockedEntities.RATING_1))
            .when(repository)
            .findById(MockedEntities.RATING_1.getId());
        Mockito
            .doReturn(new PageImpl<>(MockedEntities.RATINGS))
            .when(repository)
            .findAll(Mockito.any(Pageable.class));
        Mockito
            .doReturn(new PageImpl<>(MockedEntities.RATINGS))
            .when(repository)
            .findAllFiltered(
                Mockito.any(RatingFilter.class),
                Mockito.any(Pageable.class)
            );
        Mockito
            .doReturn(MockedEntities.RATING_1)
            .when(repository)
            .save(MockedEntities.RATING_1);
    }
}
