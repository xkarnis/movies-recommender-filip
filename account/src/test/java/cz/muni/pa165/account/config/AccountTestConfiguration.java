package cz.muni.pa165.account.config;

import cz.muni.pa165.account.MockedEntities;
import cz.muni.pa165.account.data.repository.AccountRepository;
import cz.muni.pa165.account.service.AccountService;
import cz.muni.pa165.core.exception.ResourceNotFoundException;
import jakarta.annotation.PostConstruct;
import java.util.Optional;
import org.mockito.Mockito;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@TestConfiguration
public class AccountTestConfiguration {

    private final AccountRepository repository = Mockito.mock(
        AccountRepository.class
    );

    @Bean
    public AccountService accountService() {
        return new AccountService(repository);
    }

    @PostConstruct
    public void setup() {
        mockRepositoryMethods();
    }

    private void mockRepositoryMethods() {
        Mockito
            .doThrow(ResourceNotFoundException.class)
            .when(repository)
            .findById(Mockito.anyLong());
        Mockito
            .doReturn(Optional.of(MockedEntities.JOHN_WICK))
            .when(repository)
            .findById(MockedEntities.JOHN_WICK.getId());
        Mockito
            .doReturn(new PageImpl<>(MockedEntities.ACCOUNTS))
            .when(repository)
            .findAll(Mockito.any(Pageable.class));
        Mockito
            .doReturn(MockedEntities.JOHN_WICK)
            .when(repository)
            .save(MockedEntities.JOHN_WICK);
    }
}
