package cz.muni.pa165.account.service;

import cz.muni.pa165.account.MockedEntities;
import cz.muni.pa165.account.config.AccountTestConfiguration;
import cz.muni.pa165.core.exception.ResourceNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@Import(AccountTestConfiguration.class)
@ActiveProfiles("test")
class AccountServiceTest {

    @Autowired
    AccountService service;

    @Test
    void findByIdExisting() {
        Assertions.assertSame(
            MockedEntities.JOHN_WICK,
            service.findById(MockedEntities.JOHN_WICK.getId())
        );
    }

    @Test
    void findByIdNotExisting() {
        Assertions.assertThrows(
            ResourceNotFoundException.class,
            () -> service.findById(4L)
        );
    }

    @Test
    void findAll() {
        Assertions.assertEquals(
            new PageImpl<>(MockedEntities.ACCOUNTS),
            service.findAll(PageRequest.of(0, 10))
        );
    }

    @Test
    void create() {
        Assertions.assertSame(
            MockedEntities.JOHN_WICK,
            service.create(MockedEntities.JOHN_WICK)
        );
    }

    @Test
    void update() {
        Assertions.assertSame(
            MockedEntities.JOHN_WICK,
            service.update(MockedEntities.JOHN_WICK)
        );
    }

    @Test
    void updateNotExisting() {
        Assertions.assertThrows(
            ResourceNotFoundException.class,
            () -> service.update(MockedEntities.HABI_MOTTER)
        );
    }

    @Test
    void delete() {
        service.delete(MockedEntities.JOHN_WICK.getId());
    }
}
