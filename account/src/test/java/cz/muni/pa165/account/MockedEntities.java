package cz.muni.pa165.account;

import cz.muni.pa165.account.data.model.Account;
import java.util.List;

public class MockedEntities {

    public static final Account JOHN_WICK = new Account(
        1L,
        "John Wick",
        "johnlikespuppies@gmail.com",
        true,
        "xxx",
        "sugar"
    );

    public static final Account HABI_MOTTER = new Account(
        2L,
        "Habi Motter",
        "secretvoldemort@gmail.com",
        false,
        "yyy",
        "scar"
    );

    public static final Account JOHN_SNOW = new Account(
        3L,
        "John Snow",
        "winterbad@gmail.com",
        false,
        "zzz",
        "ice"
    );

    public static final List<Account> ACCOUNTS = List.of(
        JOHN_WICK,
        HABI_MOTTER,
        JOHN_SNOW
    );
}
