package cz.muni.pa165.account.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.account.data.model.Account;
import cz.muni.pa165.account.data.repository.AccountRepository;
import cz.muni.pa165.core.util.CommonUtils;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DataSeederService {

    private final AccountRepository accountRepository;

    @Value("${rest.data.seed.accounts}")
    private String accountsSeedFile;

    @Autowired
    public DataSeederService(AccountRepository accountRepository) {
        this.accountRepository = accountRepository;
    }

    public void clearData() {
        accountRepository.deleteAll();
        accountRepository.flush();
    }

    public void seedData() {
        clearData();
        List<Account> accounts = CommonUtils.loadSeedJson(
            new ObjectMapper(),
            accountsSeedFile,
            new TypeReference<>() {}
        );
        accountRepository.saveAll(accounts);
    }
}
