package cz.muni.pa165.account.mappers;

import cz.muni.pa165.account.data.model.Account;
import cz.muni.pa165.core.api.account.AccountDto;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

@Mapper(componentModel = "spring")
public interface AccountMapper {
    AccountDto mapToDto(Account account);

    List<AccountDto> mapToList(List<Account> accounts);

    default Page<AccountDto> mapToPageDto(Page<Account> accounts) {
        return new PageImpl<>(
            mapToList(accounts.getContent()),
            accounts.getPageable(),
            accounts.getTotalPages()
        );
    }

    Account mapToEntity(AccountDto account);
}
