package cz.muni.pa165.account.rest;

import cz.muni.pa165.account.facade.AccountFacade;
import cz.muni.pa165.core.api.account.AccountDto;
import cz.muni.pa165.core.constants.AppSecurityScheme;
import cz.muni.pa165.core.constants.OAuthScopes;
import io.micrometer.observation.Observation;
import io.micrometer.observation.ObservationRegistry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.server.resource.introspection.OAuth2IntrospectionAuthenticatedPrincipal;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "${rest.path.accounts}")
public class AccountRestController {

    private final AccountFacade accountFacade;
    private final ObservationRegistry observationRegistry;

    @Autowired
    public AccountRestController(
        AccountFacade accountFacade,
        ObservationRegistry observationRegistry
    ) {
        this.accountFacade = accountFacade;
        this.observationRegistry = observationRegistry;
    }

    @Operation(
        summary = "Get current user's account",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_READ }
        )
    )
    @GetMapping("/me")
    @ResponseStatus(HttpStatus.OK)
    public AccountDto findMyAccount(
        @AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal principal
    ) {
        String email = principal.getAttribute("sub");

        return Observation
            .createNotStarted("account.findMyAccount", observationRegistry)
            .lowCardinalityKeyValue("email", String.valueOf(email))
            .observe(() -> accountFacade.findByEmail(email));
    }

    @Operation(
        summary = "Register an account",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        )
    )
    @PostMapping("/register")
    @ResponseStatus(HttpStatus.CREATED)
    public AccountDto register(
        @RequestBody AccountDto account,
        @AuthenticationPrincipal OAuth2IntrospectionAuthenticatedPrincipal principal
    ) {
        String email = principal.getAttribute("sub");
        account.setEmail(email);

        return Observation
            .createNotStarted("account.register", observationRegistry)
            .lowCardinalityKeyValue("account", account.getAccountName())
            .observe(() -> accountFacade.create(account));
    }

    @Operation(summary = "Get account by ID")
    @GetMapping(path = "${rest.path.id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AccountDto> findById(@PathVariable Long id) {
        final AccountDto dto = Observation
            .createNotStarted("account.findById", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .observe(() -> accountFacade.findById(id));
        return ResponseEntity.ok(dto);
    }

    @Operation(
        summary = "Get all accounts",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_READ }
        )
    )
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public Page<AccountDto> findAll(@ParameterObject Pageable pageable) {
        return Observation
            .createNotStarted("account.findAll", observationRegistry)
            .lowCardinalityKeyValue(
                "page",
                String.valueOf(pageable.getPageNumber())
            )
            .lowCardinalityKeyValue(
                "size",
                String.valueOf(pageable.getPageSize())
            )
            .observe(() -> accountFacade.findAll(pageable));
    }

    @Operation(
        summary = "Create an account",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        )
    )
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AccountDto createAccount(@RequestBody AccountDto account) {
        return Observation
            .createNotStarted("account.createAccount", observationRegistry)
            .lowCardinalityKeyValue("account", account.getAccountName())
            .observe(() -> accountFacade.create(account));
    }

    @Operation(
        summary = "Update an account by ID",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        )
    )
    @PutMapping(path = "${rest.path.id}")
    @ResponseStatus(HttpStatus.OK)
    public AccountDto updateAccount(
        @PathVariable Long id,
        @RequestBody AccountDto account
    ) {
        return Observation
            .createNotStarted("account.updateAccount", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .lowCardinalityKeyValue("account", account.getAccountName())
            .observe(() -> accountFacade.update(id, account));
    }

    @Operation(
        summary = "Delete account by ID",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        )
    )
    @DeleteMapping(path = "${rest.path.id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable Long id) {
        Observation
            .createNotStarted("account.deleteById", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .observe(() -> accountFacade.deleteById(id));
    }
}
