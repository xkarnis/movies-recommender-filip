package cz.muni.pa165.account;

import cz.muni.pa165.account.config.SecurityConfig;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.info.License;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@OpenAPIDefinition(
    info = @Info(
        title = "Account Service",
        version = "1.0",
        description = "Simple microservice for CRUD of Accounts.",
        contact = @Contact(name = "Pavol Baran", email = "492953@mail.muni.cz"),
        license = @License(
            name = "Apache 2.0",
            url = "https://www.apache.org/licenses/LICENSE-2.0.html"
        )
    )
)
@ComponentScan(basePackages = "cz.muni.pa165")
@EnableJpaRepositories
@SpringBootApplication
public class AccountMicroservice {

    SecurityConfig securityConfig;

    @Autowired
    AccountMicroservice(SecurityConfig securityConfig) {
        this.securityConfig = securityConfig;
    }

    public static void main(String[] args) {
        SpringApplication.run(AccountMicroservice.class, args);
    }
}
