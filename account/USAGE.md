# Account microservice
Represents a microservice for managing user accounts. Provides a REST API that allows for CRUD operations on accounts.

## Running the application
The microservice runs by default on `port 8080`. To run the microservice:
```shell
mvn clean install # compile the project first
mvn spring-boot:run
```
To run the microservice with custom port in case of port conflicts:
```shell
mvn spring-boot:run -Dspring-boot.run.arguments="--server.port=8000"
```

## Endpoints

### [GET] /swagger-ui/index.html
Swagger UI for the microservice. Open this in your browser to see the Open API documentation: <http://localhost:8000/swagger-ui/index.html>

### [GET] /api/v1/accounts/{id}
Get account with specific ID.
```shell
curl localhost:8000/api/v1/accounts/1
```
Response:
```json
{
  "id": 1,
  "accountName": "Habi Motter",
  "email": "secretvoldemort@gmail.com",
  "admin" : "false"
}
```

### [GET] /api/v1/accounts
Get all saved accounts.
```shell
curl localhost:8000/api/v1/accounts
```
Response:
```json
{
  "content": [
    {
      "id": 1,
      "accountName": "John Wick",
      "email": "johnlikespuppies@gmail.com",
      "admin" : "true"
    },
    {
      "id": 2,
      "accountName": "Habi Motter",
      "email": "secretvoldemort@gmail.com",
      "admin" : "false"
    },
    {
      "id": 3,
      "accountName": "John Snow",
      "email": "winterbad@gmail.com",
      "admin" : "false"
    }
  ],
  "pageable": {
    "sort": {
      "empty": true,
      "sorted": false,
      "unsorted": true
    },
    "offset": 0,
    "pageNumber": 0,
    "pageSize": 20,
    "paged": true,
    "unpaged": false
  },
  "last": true,
  "totalPages": 1,
  "totalElements": 3,
  "size": 20,
  "number": 0,
  "sort": {
    "empty": true,
    "sorted": false,
    "unsorted": true
  },
  "first": true,
  "numberOfElements": 3,
  "empty": false
}
```

### [POST] /api/v1/accounts
Create an account.
```shell
curl localhost:8000/api/v1/accounts -X POST -H "Content-Type: application/json" -d '{"id":null,"accountName":"rick","email":"getschwifty@gmail.com"}'
```
Response:
```json
{
  "id": 4,
  "accountName": "rick",
  "email": "getschwifty@gmail.com",
  "admin" : "false"
}
```

### [PUT] /api/v1/accounts/{id}
Update an account with specific ID.
```shell
curl localhost:8000/api/v1/accounts/4 -X PUT -H "Content-Type: application/json" -d '{"id":null,"accountName":"morty","email":"getschwifty@gmail.com"}'
```
Response:
```json
{
  "id": 4,
  "accountName": "morty",
  "email": "getschwifty@gmail.com",
  "admin" : "false"
}
```

### [DELETE] /api/v1/accounts/{id}
Delete an account with specific ID.
```shell
curl localhost:8000/api/v1/accounts/1 -X DELETE
```