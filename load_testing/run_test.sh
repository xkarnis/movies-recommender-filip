#!/bin/bash

docker-compose --file ../docker-compose-insecure.yaml up -d # start services

echo "waiting for services too boot..."
sleep 30

for PORT in {8000,8001,8002,8003} ; do
    curl "localhost:$PORT/data" -X PUT # seed each database with data
done

locust # run load test
