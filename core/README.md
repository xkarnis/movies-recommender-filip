# Core module

The core module contains the common functionality of the application.

## How to use in other modules
1. Add the dependency to the module's `pom.xml` file:
    ```xml
    <dependency>
        <groupId>cz.muni.pa165</groupId>
        <artifactId>core</artifactId>
        <version>${project.version}</version>
    </dependency>
    ```
2. Scan for the beans:
    ```java
    @ComponentScan(basePackages = "cz.muni.pa165.core")
    ```
