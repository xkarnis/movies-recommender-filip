package cz.muni.pa165.core.constants;

public class AppSecurityScheme {

    public static final String BEARER = "bearer";
    public static final String NAME = "bearerAuth";
    public static final String DESCRIPTION =
        "Use access token from oauth2 server";
}
