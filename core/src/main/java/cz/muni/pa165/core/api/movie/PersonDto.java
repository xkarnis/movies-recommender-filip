package cz.muni.pa165.core.api.movie;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Schema(title = "Person", description = "Person in the movie database")
public class PersonDto {

    @Schema(
        description = "ID",
        type = "integer",
        format = "int64",
        example = "1"
    )
    private Long id;

    @NotEmpty
    @Schema(description = "Name", example = "Johnny")
    private String name;

    @NotEmpty
    @Schema(description = "Surname", example = "Depp")
    private String surname;
}
