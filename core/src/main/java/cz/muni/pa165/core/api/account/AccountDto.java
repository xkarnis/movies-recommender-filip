package cz.muni.pa165.core.api.account;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Schema(title = "User account", description = "Account of a user")
@AllArgsConstructor
@NoArgsConstructor
@Data
public class AccountDto {

    @Schema(
        description = "ID",
        type = "integer",
        format = "int64",
        example = "1"
    )
    private Long id;

    @NotEmpty
    @Schema(description = "Username of the account", example = "Johnny")
    private String accountName;

    @NotEmpty
    @Schema(
        description = "Email associated with the account",
        example = "wickiestofthemall@gmail.com"
    )
    private String email;

    @NotEmpty
    @Schema(
        description = "Flag indicating whether the account is an admin",
        example = "false"
    )
    private boolean isAdmin;
}
