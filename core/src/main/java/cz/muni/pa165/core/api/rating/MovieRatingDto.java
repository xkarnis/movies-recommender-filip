package cz.muni.pa165.core.api.rating;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotNull;
import lombok.Data;

@Data
@Schema(
    title = "Movie rating",
    description = "Rating of a movie given by a user. All ratings are in range 0-100."
)
public class MovieRatingDto {

    @Schema(
        description = "ID",
        type = "integer",
        format = "int64",
        example = "1"
    )
    private Long id;

    @NotNull
    @Schema(
        description = "ID of the movie this rating is for",
        type = "integer",
        format = "int64",
        example = "1"
    )
    private Long movieId;

    @Schema(
        description = "ID of the user who rated the movie",
        type = "integer",
        format = "int64",
        example = "1"
    )
    private Long userId;

    @Schema(description = "Rating for acting", example = "87")
    private Integer acting;

    @Schema(description = "Rating for plot", example = "87")
    private Integer plot;

    @Schema(description = "Rating for visuals", example = "87")
    private Integer visuals;

    @Schema(description = "Rating for sound", example = "87")
    private Integer sound;

    @Schema(description = "Rating for special effects", example = "87")
    private Integer sfx;

    @Schema(description = "Overall rating", example = "87")
    private Integer overall;

    @Schema(description = "Comment", example = "This movie was awesome!")
    private String comment;
}
