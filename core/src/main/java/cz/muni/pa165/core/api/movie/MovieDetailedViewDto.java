package cz.muni.pa165.core.api.movie;

import cz.muni.pa165.core.api.genre.GenreDto;
import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import java.util.Set;
import lombok.Data;

@Data
@Schema(title = "Movie detailed view", description = "Detailed view of a movie")
public class MovieDetailedViewDto {

    @Schema(
        description = "ID",
        type = "integer",
        format = "int64",
        example = "1"
    )
    private Long id;

    @NotEmpty
    @Schema(description = "Title", example = "Pirates of the Caribbean")
    private String title;

    @Schema(
        description = "Description",
        example = "A pirate captain and his crew are in search of a legendary treasure"
    )
    private String description;

    @Schema(
        description = "Director",
        example = "{\"id\": 1, \"name\": \"Gore\", \"surname\": \"Verbinski\"}"
    )
    private PersonDto director;

    @Schema(
        description = "List of actors",
        example = "[{\"id\": 1, \"name\": \"Johnny\", \"surname\": \"Depp\"}, {\"id\": 2, \"name\": \"Orlando\", \"surname\": \"Bloom\"}]"
    )
    private Set<PersonDto> actors;

    @Schema(
        description = "List of genres",
        example = "[{id: 1, name: \"Action\"}, {id: 3, name: \"Adventure\"}]"
    )
    private Set<GenreDto> genres;

    @Schema(
        description = "List of image URLs",
        example = "[\"https://example.com/image1.jpg\", \"https://example.com/image2.jpg\"]"
    )
    private Set<String> images;
}
