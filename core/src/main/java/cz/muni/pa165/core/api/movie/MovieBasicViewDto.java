package cz.muni.pa165.core.api.movie;

import io.swagger.v3.oas.annotations.media.Schema;
import jakarta.validation.constraints.NotEmpty;
import lombok.Data;

@Data
@Schema(title = "Movie basic view", description = "Basic view of a movie")
public class MovieBasicViewDto {

    @Schema(
        description = "ID",
        type = "integer",
        format = "int64",
        example = "1"
    )
    private Long id;

    @NotEmpty
    @Schema(description = "Title", example = "Pirates of the Caribbean")
    private String title;

    @Schema(
        description = "Description",
        example = "A pirate captain and his crew are in search of a legendary treasure"
    )
    private String description;
}
