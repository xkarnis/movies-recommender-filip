package cz.muni.pa165.movie.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.api.rating.MovieRatingDto;
import cz.muni.pa165.core.api.rating.RatingsPageableResponse;
import cz.muni.pa165.movie.data.model.Movie;
import cz.muni.pa165.movie.data.repository.MovieRepository;
import java.io.IOException;
import java.util.List;
import okhttp3.mockwebserver.MockResponse;
import okhttp3.mockwebserver.MockWebServer;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

@ActiveProfiles("test")
@SpringBootTest
public class RecommendationServiceTest {

    private static MockWebServer mockWebServer;
    private static WebClientRatingService ratingService;

    @Mock
    protected MovieRepository repository;

    private final ObjectMapper mapper;

    @Autowired
    RecommendationServiceTest(ObjectMapper mapper) {
        this.mapper = mapper;
    }

    @BeforeAll
    static void setUp() throws IOException {
        mockWebServer = new MockWebServer();
        mockWebServer.start();
        ratingService =
            new WebClientRatingService(
                "http://localhost",
                Integer.toString(mockWebServer.getPort()),
                "rating"
            );
    }

    @AfterAll
    static void tearDown() throws IOException {
        mockWebServer.shutdown();
    }

    @Test
    public void recommend() throws Exception {
        //given
        var movie1 = new Movie();
        movie1.setId(1L);

        // first two ratings are for the same movie1
        var ratingDto1 = new MovieRatingDto();
        ratingDto1.setId(1L);
        ratingDto1.setUserId(11L);
        ratingDto1.setMovieId(1L);

        var ratingDto2 = new MovieRatingDto();
        ratingDto2.setId(2L);
        ratingDto2.setUserId(22L);
        ratingDto2.setMovieId(1L);

        // third and fourth ratings are for different movie by the same users
        // these should be shown as recommended
        var ratingDto3 = new MovieRatingDto();
        ratingDto3.setId(3L);
        ratingDto3.setUserId(11L);
        ratingDto3.setMovieId(2L);

        var ratingDto4 = new MovieRatingDto();
        ratingDto4.setId(4L);
        ratingDto4.setUserId(22L);
        ratingDto4.setMovieId(3L);

        var mockedRatingsPage = new RatingsPageableResponse(
            List.of(ratingDto1, ratingDto2),
            1,
            1,
            2L
        );
        var mockedRatingsPage2 = new RatingsPageableResponse(
            List.of(ratingDto1, ratingDto2, ratingDto3, ratingDto4),
            1,
            1,
            4L
        );

        // Mock first request which gets ratings for specific movie
        mockWebServer.enqueue(
            new MockResponse()
                .setBody(mapper.writeValueAsString(mockedRatingsPage))
                .addHeader("Content-Type", "application/json")
        );

        // Mock second request which gets ratings of all users who liked the movie from the first request
        mockWebServer.enqueue(
            new MockResponse()
                .setBody(mapper.writeValueAsString(mockedRatingsPage2))
                .addHeader("Content-Type", "application/json")
        );

        // Finally mock repository to return movie list
        Mockito
            .doReturn(List.of(movie1))
            .when(repository)
            .findAllById(
                List.of(ratingDto3.getMovieId(), ratingDto4.getMovieId())
            );

        //when
        var recommendationService = new RecommendationService(
            repository,
            ratingService,
            1,
            2
        );
        var movieList = recommendationService.recommend(1L);

        //then
        Assertions.assertEquals(
            "/rating?movieId=" + movie1.getId() + "&overall=1&page=0&size=10",
            mockWebServer.takeRequest().getPath()
        );
        Assertions.assertEquals(
            "/rating?userIds=" +
            ratingDto1.getUserId() +
            "&userIds=" +
            ratingDto2.getUserId() +
            "&overall=1&page=0&size=10",
            mockWebServer.takeRequest().getPath()
        );
        Assertions.assertEquals(1, movieList.size());
        Assertions.assertEquals(movie1.getId(), movieList.get(0).getId());
    }
}
