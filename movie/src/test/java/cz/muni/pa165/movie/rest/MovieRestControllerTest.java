package cz.muni.pa165.movie.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.api.movie.MovieDetailedViewDto;
import cz.muni.pa165.movie.config.MovieTestConfiguration;
import cz.muni.pa165.movie.facade.MovieFacade;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@WebMvcTest(MovieRestController.class)
@OverrideAutoConfiguration(enabled = true)
@Import(MovieTestConfiguration.class)
@ActiveProfiles("test")
@AutoConfigureMockMvc(addFilters = false)
public class MovieRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private MovieFacade movieFacade;

    @Value("${rest.path.movies}")
    private String restPath;

    @Test
    void findAllTest() throws Exception {
        mockMvc
            .perform(
                get(restPath)
                    .param("page", "0")
                    .param("size", "10")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }

    @Test
    void findByIdTest() throws Exception {
        mockMvc
            .perform(
                get(restPath + "/1")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }

    @Test
    void createTest() throws Exception {
        MovieDetailedViewDto movieDto = new MovieDetailedViewDto();
        movieDto.setTitle("title");

        mockMvc
            .perform(
                post(restPath)
                    .content(objectMapper.writeValueAsString(movieDto))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isCreated());
    }

    @Test
    void updateTest() throws Exception {
        MovieDetailedViewDto movieDto = new MovieDetailedViewDto();
        movieDto.setTitle("title");

        mockMvc
            .perform(
                put(restPath + "/1")
                    .content(objectMapper.writeValueAsString(movieDto))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }

    @Test
    void deleteTest() throws Exception {
        mockMvc
            .perform(
                delete(restPath + "/1")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNoContent());
    }
}
