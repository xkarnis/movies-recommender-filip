package cz.muni.pa165.movie.service;

import cz.muni.pa165.core.api.rating.MovieRatingDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RatingService {

    private final WebClientRatingService webClientRatingService;

    @Autowired
    public RatingService(WebClientRatingService webClientRatingService) {
        this.webClientRatingService = webClientRatingService;
    }

    @Transactional
    public MovieRatingDto createRating(MovieRatingDto movieRatingDto) {
        return webClientRatingService.createRating(movieRatingDto);
    }
}
