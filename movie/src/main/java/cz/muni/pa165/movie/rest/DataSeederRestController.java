package cz.muni.pa165.movie.rest;

import cz.muni.pa165.movie.service.DataSeederService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

/**
 * Spring REST Controller for seeding and clearing the database table of movies and people.
 */
@Slf4j
@RestController
@RequestMapping(path = "${rest.data.api}")
public class DataSeederRestController {

    private final DataSeederService dataSeederService;

    @Autowired
    DataSeederRestController(DataSeederService dataSeederService) {
        this.dataSeederService = dataSeederService;
    }

    @Operation(
        summary = "Delete all movies and people.",
        description = """
                    Removes all (not just seeded) movie and person entries from the database.
                    """
    )
    @DeleteMapping
    @CrossOrigin(origins = "*")
    @ResponseStatus(HttpStatus.OK)
    public void clearData(HttpServletRequest req) {
        log.info(
            "{} {} called from {}",
            req.getMethod(),
            req.getRequestURI(),
            req.getRemoteHost()
        );
        dataSeederService.clearData();
    }

    @Operation(
        summary = "Seeds the movie and person tables.",
        description = """
                    Seeds the movie and person tables with predefined values. Note that the table is
                    cleared before seeding. This means the operation is idempotent.
                    """
    )
    @PutMapping
    @CrossOrigin(origins = "*")
    @ResponseStatus(HttpStatus.OK)
    public void seedData(HttpServletRequest req) {
        log.info(
            "{} {} called from {}",
            req.getMethod(),
            req.getRequestURI(),
            req.getRemoteHost()
        );
        dataSeederService.seedData();
    }
}
