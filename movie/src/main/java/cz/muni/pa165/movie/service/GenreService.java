package cz.muni.pa165.movie.service;

import cz.muni.pa165.core.api.genre.GenreDto;
import cz.muni.pa165.movie.data.model.Movie;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class GenreService {

    private final WebClientGenreService webClientGenreService;

    @Autowired
    public GenreService(WebClientGenreService webClientGenreService) {
        this.webClientGenreService = webClientGenreService;
    }

    public Set<GenreDto> getGenresFromMovie(Movie movie) {
        return webClientGenreService
            .getGenres()
            .stream()
            .flatMap(pageableResponse -> pageableResponse.getContent().stream())
            .filter(genreDto -> movie.getGenreIds().contains(genreDto.getId()))
            .collect(Collectors.toSet());
    }
}
