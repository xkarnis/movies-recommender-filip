package cz.muni.pa165.movie.facade;

import cz.muni.pa165.core.api.genre.GenreDto;
import cz.muni.pa165.core.api.movie.MovieBasicViewDto;
import cz.muni.pa165.core.api.movie.MovieDetailedViewDto;
import cz.muni.pa165.core.api.rating.MovieRatingDto;
import cz.muni.pa165.movie.data.model.Movie;
import cz.muni.pa165.movie.data.repository.MovieFilter;
import cz.muni.pa165.movie.mapper.MovieMapper;
import cz.muni.pa165.movie.service.GenreService;
import cz.muni.pa165.movie.service.MovieService;
import cz.muni.pa165.movie.service.RatingService;
import cz.muni.pa165.movie.service.RecommendationService;
import java.util.List;
import java.util.Set;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class MovieFacade {

    private final MovieService movieService;
    private final MovieMapper movieMapper;
    private final GenreService genreService;
    private final RecommendationService recommendationService;
    private final RatingService ratingService;

    @Autowired
    public MovieFacade(
        MovieService movieService,
        MovieMapper movieMapper,
        GenreService genreService,
        RecommendationService recommendationService,
        RatingService ratingService
    ) {
        this.movieService = movieService;
        this.movieMapper = movieMapper;
        this.genreService = genreService;
        this.recommendationService = recommendationService;
        this.ratingService = ratingService;
    }

    @Transactional(readOnly = true)
    public MovieDetailedViewDto findById(Long id) {
        final Movie movie = movieService.findById(id);
        final MovieDetailedViewDto movieDto = movieMapper.mapToDetailViewDto(
            movie
        );
        final Set<GenreDto> genreDtos = genreService.getGenresFromMovie(movie);
        movieDto.setGenres(genreDtos);
        return movieDto;
    }

    @Transactional(readOnly = true)
    public Page<MovieBasicViewDto> findAll(
        Pageable pageable,
        MovieFilter movieFilter
    ) {
        return movieMapper.mapToPageDto(
            movieService.findAll(pageable, movieFilter)
        );
    }

    public MovieDetailedViewDto create(MovieDetailedViewDto movie) {
        return movieMapper.mapToDetailViewDto(
            movieService.create(movieMapper.mapToEntity(movie))
        );
    }

    public MovieDetailedViewDto update(Long id, MovieDetailedViewDto movie) {
        movie.setId(id);
        return movieMapper.mapToDetailViewDto(
            movieService.update(movieMapper.mapToEntity(movie))
        );
    }

    public void delete(Long id) {
        movieService.delete(movieService.findById(id));
    }

    public List<MovieBasicViewDto> recommend(Long id) {
        return movieMapper.mapToList(recommendationService.recommend(id));
    }

    public MovieRatingDto createRating(MovieRatingDto movieRatingDto) {
        return ratingService.createRating(movieRatingDto);
    }
}
