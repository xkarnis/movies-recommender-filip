package cz.muni.pa165.movie.data.repository;

import lombok.Data;

@Data
public class MovieFilter {

    private String title;
    private String directorSurname;
    private String actorSurname;

    public MovieFilter() {
        title = "";
        directorSurname = "";
        actorSurname = "";
    }

    public boolean isEmpty() {
        return (
            (title == null || title.isEmpty()) &&
            (directorSurname == null || directorSurname.isEmpty()) &&
            (actorSurname == null || actorSurname.isEmpty())
        );
    }
}
