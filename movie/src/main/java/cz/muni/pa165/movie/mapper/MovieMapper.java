package cz.muni.pa165.movie.mapper;

import cz.muni.pa165.core.api.movie.MovieBasicViewDto;
import cz.muni.pa165.core.api.movie.MovieDetailedViewDto;
import cz.muni.pa165.movie.data.model.Movie;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

@Mapper(componentModel = "spring", uses = PersonMapper.class)
public interface MovieMapper {
    MovieBasicViewDto mapToDto(Movie movie);

    MovieDetailedViewDto mapToDetailViewDto(Movie movie);

    List<MovieBasicViewDto> mapToList(List<Movie> movies);

    default Page<MovieBasicViewDto> mapToPageDto(Page<Movie> movies) {
        return new PageImpl<>(
            mapToList(movies.getContent()),
            movies.getPageable(),
            movies.getTotalPages()
        );
    }

    Movie mapToEntity(MovieDetailedViewDto movie);
}
