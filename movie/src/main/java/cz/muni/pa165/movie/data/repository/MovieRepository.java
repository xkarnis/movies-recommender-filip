package cz.muni.pa165.movie.data.repository;

import cz.muni.pa165.movie.data.model.Movie;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MovieRepository extends JpaRepository<Movie, Long> {
    Page<Movie> findByTitleContainingAndDirector_SurnameContainingAndActors_SurnameContaining(
        Pageable pageable,
        String title,
        String directorSurname,
        String actorSurname
    );
}
