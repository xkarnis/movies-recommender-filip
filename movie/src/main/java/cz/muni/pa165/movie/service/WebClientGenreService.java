package cz.muni.pa165.movie.service;

import cz.muni.pa165.core.api.genre.GenreDtoPageableResponse;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.server.resource.web.reactive.function.client.ServletBearerExchangeFilterFunction;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class WebClientGenreService {

    private final String genresPath;

    private final WebClient genreWebClient;

    public WebClientGenreService(
        @Value("${service.genre.host}") String genreMicroserviceHost,
        @Value("${service.genre.port}") String genreMicroservicePort,
        @Value("${service.genre.rest.path.genres}") String genresPath
    ) {
        this.genresPath = genresPath;
        genreWebClient =
            WebClient
                .builder()
                .filter(new ServletBearerExchangeFilterFunction())
                .baseUrl(genreMicroserviceHost + ":" + genreMicroservicePort)
                .build();
    }

    public List<GenreDtoPageableResponse> getGenres() {
        // TODO: handle errors
        return genreWebClient
            .get()
            .uri(genresPath)
            .retrieve()
            .bodyToFlux(GenreDtoPageableResponse.class)
            .collectList()
            .block();
    }
}
