package cz.muni.pa165.movie.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.util.CommonUtils;
import cz.muni.pa165.movie.data.model.Movie;
import cz.muni.pa165.movie.data.model.Person;
import cz.muni.pa165.movie.data.repository.MovieRepository;
import cz.muni.pa165.movie.data.repository.PersonRepository;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class DataSeederService {

    private final PersonRepository personRepository;
    private final MovieRepository movieRepository;

    @Value("${rest.data.seed.people}")
    private String peopleSeedFile;

    @Value("${rest.data.seed.movies}")
    private String moviesSeedFile;

    @Autowired
    public DataSeederService(
        PersonRepository personRepository,
        MovieRepository movieRepository
    ) {
        this.personRepository = personRepository;
        this.movieRepository = movieRepository;
    }

    public void clearData() {
        personRepository.deleteAll();
        movieRepository.deleteAll();
        personRepository.flush();
        movieRepository.flush();
    }

    public void seedData() {
        clearData();

        List<Person> people = CommonUtils.loadSeedJson(
            new ObjectMapper(),
            peopleSeedFile,
            new TypeReference<>() {}
        );
        List<Movie> movies = CommonUtils.loadSeedJson(
            new ObjectMapper(),
            moviesSeedFile,
            new TypeReference<>() {}
        );

        // we need to first save the entities without links between them
        movieRepository.saveAll(movies);
        personRepository.saveAll(people);

        Random rand = new Random(42);
        for (Movie movie : movies) {
            // set random director
            var pickedDirector = people.get(rand.nextInt(people.size()));
            pickedDirector.getMoviesDirected().add(movie);
            movie.setDirector(pickedDirector);

            // set actors, for each movie at most 10 random actors
            for (int i = 0; i < rand.nextInt(10); i++) {
                var pickedActor = people.get(rand.nextInt(people.size()));
                pickedActor.getMoviesActedIn().add(movie);
                movie.getActors().add(pickedActor);
            }
            //for now just set the same genre ids for each movie
            var genreIds = new HashSet<>(List.of(1L, 2L, 3L));
            movie.setGenreIds(genreIds);
        }

        personRepository.saveAll(people);
        movieRepository.saveAll(movies);
    }
}
