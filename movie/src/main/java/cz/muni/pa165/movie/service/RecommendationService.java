package cz.muni.pa165.movie.service;

import cz.muni.pa165.core.api.rating.MovieRatingDto;
import cz.muni.pa165.core.api.rating.RatingsPageableResponse;
import cz.muni.pa165.movie.data.model.Movie;
import cz.muni.pa165.movie.data.repository.MovieRepository;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class RecommendationService {

    private final WebClientRatingService webClientRatingService;
    private final int positivityThreshold;
    private final int recommendationCount;
    private final MovieRepository movieRepository;

    @Autowired
    public RecommendationService(
        MovieRepository movieRepository,
        WebClientRatingService webClientRatingService,
        @Value(
            "${recommendations.positivityThreshold}"
        ) int positivityThreshold,
        @Value("${recommendations.recommendationCount}") int recommendationCount
    ) {
        this.webClientRatingService = webClientRatingService;
        this.positivityThreshold = positivityThreshold;
        this.recommendationCount = recommendationCount;
        this.movieRepository = movieRepository;
    }

    @Transactional(readOnly = true)
    public List<Movie> recommend(Long id) {
        List<Long> recommendedMovieIds = new ArrayList<>();
        boolean hasMorePages = true;
        int movieRatingsPage = 0;

        while (
            hasMorePages && recommendedMovieIds.size() < recommendationCount
        ) {
            RatingsPageableResponse ratings =
                webClientRatingService.getMovieRatings(
                    id,
                    positivityThreshold,
                    movieRatingsPage
                );

            List<Long> likedByUsers = ratings
                .stream()
                .map(MovieRatingDto::getUserId)
                .toList();

            addLikedMovies(recommendedMovieIds, likedByUsers, id);

            hasMorePages = ratings.hasNext();
            movieRatingsPage++;
        }

        return movieRepository.findAllById(recommendedMovieIds);
    }

    private void addLikedMovies(
        List<Long> recommendedMovieIds,
        List<Long> likedByUsers,
        Long movieId
    ) {
        boolean hasMoreUserRatingsPages = true;
        int userRatingsPage = 0;

        while (
            hasMoreUserRatingsPages &&
            recommendedMovieIds.size() < recommendationCount
        ) {
            RatingsPageableResponse userRatings =
                webClientRatingService.getUserRatings(
                    likedByUsers,
                    positivityThreshold,
                    userRatingsPage
                );

            recommendedMovieIds.addAll(
                userRatings
                    .stream()
                    .filter(rating -> !rating.getMovieId().equals(movieId))
                    .limit(recommendationCount - recommendedMovieIds.size())
                    .map(MovieRatingDto::getMovieId)
                    .toList()
            );

            hasMoreUserRatingsPages = userRatings.hasNext();
            userRatingsPage++;
        }
    }
}
