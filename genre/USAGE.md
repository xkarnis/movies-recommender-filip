# Genre microservice
Represents a microservice for managing movie genres. Provides a REST API that allows for CRUD operations on movie genres.

## Running the application
The microservice runs by default on `port 8080`. To run the microservice:
```shell
mvn clean install # compile the project first
mvn spring-boot:run
```
To run the microservice with custom port in case of port conflicts:
```shell
mvn spring-boot:run -Dspring-boot.run.arguments="--server.port=8001"
```

## Endpoints

### [GET] /swagger-ui/index.html
Swagger UI for the microservice. Open this in your browser to see the Open API documentation: <http://localhost:8001/swagger-ui/index.html>

### [GET] /api/v1/genres/{id}
Get genre with specific ID.
```shell
curl localhost:8001/api/v1/genres/1
```
Response:
```json
{
  "id":1,
  "name":"Action"
}
```

### [GET] /api/v1/genres
Get all saved genres.
```shell
curl localhost:8001/api/v1/genres
```
Response:
```json
[
  {
    "id": 1,
    "name": "Action"
  },
  {
    "id": 2,
    "name": "Comedy"
  },
  {
    "id": 3,
    "name": "Horror"
  },
  {
    "id": 4,
    "name": "Romance"
  },
  {
    "id": 5,
    "name": "Drama"
  }
]
```

### [POST] /api/v1/genres
Create a genre.
```shell
curl localhost:8001/api/v1/genres -X POST -H "Content-Type: application/json" -d '{"id":null,"name":"Brutal"}'
```
Response:
```json
{
  "id": 6,
  "name": "Brutal"
}
```

### [PUT] /api/v1/genres/{id}
Update a genre with specific ID.
```shell
curl localhost:8001/api/v1/genres/6 -X PUT -H "Content-Type: application/json" -d '{"id":null,"name":"Massacre"}'
```
Response:
```json
{
  "id": 6,
  "name": "Massacre"
}
```

### [DELETE] /api/v1/genres/{id}
Delete a genre with specific ID.
```shell
curl localhost:8001/api/v1/genres/8 -X DELETE
```