package cz.muni.pa165.genre.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("test")
@OverrideAutoConfiguration(enabled = true)
@WebMvcTest(DataSeedRestController.class)
@AutoConfigureMockMvc(addFilters = false)
public class DataSeederRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Value("${rest.data.api}")
    private String restPath;

    @Test
    void clearDataTest() throws Exception {
        mockMvc
            .perform(
                delete(restPath)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }

    @Test
    void seedDataTest() throws Exception {
        mockMvc
            .perform(
                put(restPath)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }
}
