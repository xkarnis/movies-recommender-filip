package cz.muni.pa165.genre;

import cz.muni.pa165.genre.data.model.Genre;
import java.util.List;

public class MockedEntities {

    public static final Genre ACTION;
    public static final Genre COMEDY;
    public static final Genre DRAMA;
    public static final Genre HORROR;
    public static final Genre ROMANCE;

    public static final List<Genre> GENRE_LIST;

    static {
        ACTION = new Genre();
        ACTION.setId(1L);
        ACTION.setName("Action");

        COMEDY = new Genre();
        COMEDY.setName("Comedy");

        DRAMA = new Genre();
        DRAMA.setName("Drama");

        HORROR = new Genre();
        HORROR.setName("Horror");

        ROMANCE = new Genre();
        ROMANCE.setName("Romance");

        GENRE_LIST = List.of(ACTION, COMEDY, DRAMA, HORROR, ROMANCE);
    }
}
