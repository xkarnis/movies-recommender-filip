package cz.muni.pa165.genre.rest;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.muni.pa165.core.api.genre.GenreDto;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.OverrideAutoConfiguration;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

@ActiveProfiles("test")
@OverrideAutoConfiguration(enabled = true)
@WebMvcTest(GenreRestController.class)
@AutoConfigureMockMvc(addFilters = false)
public class GenreRestControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper objectMapper;

    @Value("${rest.path.genres}")
    private String restPath;

    @Test
    void findAllTest() throws Exception {
        mockMvc
            .perform(
                get(restPath)
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());
    }

    @Test
    void findByIdTest() throws Exception {
        mockMvc
            .perform(
                get(restPath + "/1")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());

        mockMvc
            .perform(
                get(restPath + "/100")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNotFound());
    }

    @Test
    void createTest() throws Exception {
        GenreDto genreDto = new GenreDto();
        genreDto.setName("New Genre");

        mockMvc
            .perform(
                post(restPath)
                    .content(objectMapper.writeValueAsString(genreDto))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isCreated());
    }

    @Test
    void updateTest() throws Exception {
        GenreDto genreDto = new GenreDto();
        genreDto.setName("Updated Genre");

        mockMvc
            .perform(
                put(restPath + "/1")
                    .content(objectMapper.writeValueAsString(genreDto))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isOk());

        mockMvc
            .perform(
                put(restPath + "/100")
                    .content(objectMapper.writeValueAsString(genreDto))
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNotFound());
    }

    @Test
    void deleteTest() throws Exception {
        mockMvc
            .perform(
                delete(restPath + "/1")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNoContent());

        mockMvc
            .perform(
                delete(restPath + "/100")
                    .contentType(MediaType.APPLICATION_JSON_VALUE)
                    .accept(MediaType.APPLICATION_JSON_VALUE)
            )
            .andExpect(status().isNotFound());
    }
}
