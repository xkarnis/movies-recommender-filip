package cz.muni.pa165.genre.service;

import cz.muni.pa165.core.exception.ResourceNotFoundException;
import cz.muni.pa165.genre.MockedEntities;
import cz.muni.pa165.genre.config.MockConfiguration;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;

@Import(MockConfiguration.class)
@ActiveProfiles("test")
@SpringBootTest
class GenreServiceTest {

    private final GenreService service;

    @Autowired
    GenreServiceTest(GenreService service) {
        this.service = service;
    }

    @Test
    void findByIdExisting() {
        Assertions.assertSame(
            MockedEntities.ACTION,
            service.findById(MockedEntities.ACTION.getId())
        );
    }

    @Test
    void findByIdNotExisting() {
        Assertions.assertThrows(
            ResourceNotFoundException.class,
            () -> service.findById(2L)
        );
    }

    @Test
    void findAll() {
        Assertions.assertEquals(
            new PageImpl<>(MockedEntities.GENRE_LIST),
            service.findAll(PageRequest.of(0, 10))
        );
    }

    @Test
    void create() {
        Assertions.assertSame(
            MockedEntities.ACTION,
            service.create(MockedEntities.ACTION)
        );
    }

    @Test
    void update() {
        Assertions.assertSame(
            MockedEntities.ACTION,
            service.update(MockedEntities.ACTION)
        );
    }

    @Test
    void delete() {
        service.delete(MockedEntities.ACTION);
    }
}
