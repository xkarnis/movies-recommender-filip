package cz.muni.pa165.genre.facade;

import cz.muni.pa165.core.api.genre.GenreDto;
import cz.muni.pa165.genre.mapper.GenreMapper;
import cz.muni.pa165.genre.service.GenreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class GenreFacade {

    private final GenreService genreService;
    private final GenreMapper genreMapper;

    @Autowired
    public GenreFacade(GenreService genreService, GenreMapper genreMapper) {
        this.genreService = genreService;
        this.genreMapper = genreMapper;
    }

    @Transactional(readOnly = true)
    public GenreDto findById(Long id) {
        return genreMapper.mapToDto(genreService.findById(id));
    }

    @Transactional(readOnly = true)
    public Page<GenreDto> findAll(Pageable pageable) {
        return genreMapper.mapToPageDto(genreService.findAll(pageable));
    }

    @Transactional
    public GenreDto create(GenreDto movie) {
        return genreMapper.mapToDto(
            genreService.create(genreMapper.mapToEntity(movie))
        );
    }

    @Transactional
    public GenreDto update(GenreDto movie) {
        return genreMapper.mapToDto(
            genreService.update(genreMapper.mapToEntity(movie))
        );
    }

    @Transactional
    public void delete(Long id) {
        genreService.delete(genreService.findById(id));
    }
}
