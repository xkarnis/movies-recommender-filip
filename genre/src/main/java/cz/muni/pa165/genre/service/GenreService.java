package cz.muni.pa165.genre.service;

import cz.muni.pa165.genre.data.model.Genre;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface GenreService {
    Page<Genre> findAll(Pageable pageable);

    Genre findById(Long id);

    Genre create(Genre genre);

    Genre update(Genre genre);

    void delete(Genre genre);
}
