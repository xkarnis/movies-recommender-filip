package cz.muni.pa165.genre.rest;

import cz.muni.pa165.core.api.genre.GenreDto;
import cz.muni.pa165.core.constants.AppSecurityScheme;
import cz.muni.pa165.core.constants.OAuthScopes;
import cz.muni.pa165.genre.facade.GenreFacade;
import io.micrometer.observation.Observation;
import io.micrometer.observation.ObservationRegistry;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springdoc.core.annotations.ParameterObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

/**
 * Spring REST Controller for communication using HTTP protocol and JSON data format.
 */
@RestController
@RequestMapping(path = "${rest.path.genres}")
@Slf4j
public class GenreRestController {

    private final GenreFacade genreFacade;
    private final ObservationRegistry observationRegistry;

    @Autowired
    GenreRestController(
        GenreFacade genreFacade,
        ObservationRegistry observationRegistry
    ) {
        this.genreFacade = genreFacade;
        this.observationRegistry = observationRegistry;
    }

    @Operation(
        summary = "Get all genres",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_READ }
        ),
        description = """
                    Returns an array of all genres. The array can be paginated using the 'page' and 'size' query parameters.
                    """
    )
    @GetMapping
    @CrossOrigin(origins = "*")
    @ResponseStatus(HttpStatus.OK)
    public Page<GenreDto> getAllGenres(
        @ParameterObject Pageable pageable,
        HttpServletRequest req
    ) {
        log.info(
            "{} {} called from {}",
            req.getMethod(),
            req.getRequestURI(),
            req.getRemoteHost()
        );

        final Page<GenreDto> page = Observation
            .createNotStarted("genre.getAllGenres", observationRegistry)
            .lowCardinalityKeyValue(
                "page",
                String.valueOf(pageable.getPageNumber())
            )
            .lowCardinalityKeyValue(
                "size",
                String.valueOf(pageable.getPageSize())
            )
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> genreFacade.findAll(pageable));
        return page;
    }

    @Operation(
        summary = "Get a specific genre by its id",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_READ }
        ),
        description = """
                    Returns a genre with the given id. If the genre does not exist, the server returns a 404 status code.
                    """
    )
    @GetMapping(path = "${rest.path.id}")
    @CrossOrigin(origins = "*")
    @ResponseStatus(HttpStatus.OK)
    public GenreDto getGenre(@PathVariable Long id, HttpServletRequest req) {
        log.info(
            "{} {} called from {}",
            req.getMethod(),
            req.getRequestURI(),
            req.getRemoteHost()
        );

        final GenreDto dto = Observation
            .createNotStarted("genre.getGenre", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> genreFacade.findById(id));
        return dto;
    }

    @Operation(
        summary = "Post a new genre",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        ),
        description = """
                    Creates a new genre with the given name. If the name is empty, the server returns a 400 status code.
                    """
    )
    @PostMapping
    @CrossOrigin(origins = "*")
    @ResponseStatus(HttpStatus.CREATED)
    public GenreDto createGenre(
        @Valid @RequestBody GenreDto newGenre,
        BindingResult bindingResult,
        HttpServletRequest req
    ) {
        log.info(
            "{} {} called from {}",
            req.getMethod(),
            req.getRequestURI(),
            req.getRemoteHost()
        );

        if (bindingResult.hasErrors()) {
            log.debug("Binding errors found");

            for (FieldError fe : bindingResult.getFieldErrors()) {
                log.debug(
                    "FieldError: {} - {}",
                    fe.getField(),
                    fe.getDefaultMessage()
                );
                throw new ResponseStatusException(
                    HttpStatus.BAD_REQUEST,
                    fe.getField() + " - " + fe.getDefaultMessage()
                );
            }
        }

        final GenreDto dto = Observation
            .createNotStarted("genre.createGenre", observationRegistry)
            .lowCardinalityKeyValue("name", newGenre.getName())
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> genreFacade.create(newGenre));
        return dto;
    }

    @Operation(
        summary = "Delete a genre by ID",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        ),
        description = """
                    Deletes genre with a given ID, if it exists. If the genre does not exist, the server returns a 404 status code.
                    """
    )
    @DeleteMapping(path = "${rest.path.id}")
    @CrossOrigin(origins = "*")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteGenre(@PathVariable Long id, HttpServletRequest req) {
        log.info(
            "{} {} called from {}",
            req.getMethod(),
            req.getRequestURI(),
            req.getRemoteHost()
        );

        Observation
            .createNotStarted("genre.deleteGenre", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> genreFacade.delete(id));
    }

    @Operation(
        summary = "Update a genre by ID",
        security = @SecurityRequirement(
            name = AppSecurityScheme.NAME,
            scopes = { OAuthScopes.TEST_WRITE }
        ),
        description = """
                    Updates genre with a given ID, if it exists. If the genre does not exist, the server returns a 404 status code.
                    """
    )
    @PutMapping(path = "${rest.path.id}")
    @CrossOrigin(origins = "*")
    @ResponseStatus(HttpStatus.OK)
    public GenreDto updateGenre(
        @PathVariable Long id,
        @RequestBody GenreDto genre,
        HttpServletRequest req
    ) {
        log.info(
            "{} {} called from {}",
            req.getMethod(),
            req.getRequestURI(),
            req.getRemoteHost()
        );

        genre.setId(id);
        final GenreDto dto = Observation
            .createNotStarted("genre.updateGenre", observationRegistry)
            .lowCardinalityKeyValue("id", String.valueOf(id))
            .highCardinalityKeyValue("highTag", "highValue")
            .observe(() -> genreFacade.update(genre));
        return dto;
    }
}
