package cz.muni.pa165.genre.mapper;

import cz.muni.pa165.core.api.genre.GenreDto;
import cz.muni.pa165.genre.data.model.Genre;
import java.util.List;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;

@Mapper(componentModel = "spring")
public interface GenreMapper {
    GenreDto mapToDto(Genre genre);

    Genre mapToEntity(GenreDto genreDto);

    List<GenreDto> mapToList(List<Genre> genres);

    default Page<GenreDto> mapToPageDto(Page<Genre> genres) {
        return new PageImpl<>(
            mapToList(genres.getContent()),
            genres.getPageable(),
            genres.getTotalPages()
        );
    }
}
