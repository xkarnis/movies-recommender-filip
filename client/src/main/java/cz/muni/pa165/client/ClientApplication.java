package cz.muni.pa165.client;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.security.web.csrf.CookieCsrfTokenRepository;
import org.springframework.security.web.csrf.CsrfTokenRequestAttributeHandler;

@SpringBootApplication
public class ClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(ClientApplication.class, args);
    }

    @Bean
    public SecurityFilterChain securityFilterChain(HttpSecurity httpSecurity)
        throws Exception {
        httpSecurity
            .authorizeHttpRequests(matcherRegistry ->
                matcherRegistry
                    .requestMatchers("/", "/error")
                    .permitAll()
                    .anyRequest()
                    .authenticated()
            )
            .oauth2Login()
            .and()
            .csrf(csrfConfigurer ->
                csrfConfigurer
                    .csrfTokenRepository(
                        CookieCsrfTokenRepository.withHttpOnlyFalse()
                    )
                    .csrfTokenRequestHandler(
                        new CsrfTokenRequestAttributeHandler()
                    )
            );
        return httpSecurity.build();
    }
}
